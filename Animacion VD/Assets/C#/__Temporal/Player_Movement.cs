using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

//[CustomEditor(typeof(RagdollController))]

public class Player_Movement : MonoBehaviour
{

    Transform cam;
    [SerializeField] Animator camera_Controller;
    [SerializeField] Animator character_Controller;
    [SerializeField] private Slider staminaSlider;
    [SerializeField] private BoxCollider hitBox;
    [SerializeField] private float iFrames = 1f;
    bool _lock = false;

    bool ATK = false;
    float Stamina = 100;
    public float Hp = 200;
    private float regenTimer = 0f;
    bool isInvulnerable = false;
    bool weakSequence = false;
    bool strongSequence = false;
    int weak_ATK_Index = 0;
    int strong_ATK_Index = 0;
    [SerializeField] string[] ATK_Sequence_Weak;
    [SerializeField] string[] ATK_Sequence_Strong;
    private RagdollController ragdollController;


    private void Start()
    {
        cam = Camera.main.transform;
        ragdollController = GetComponent<RagdollController>();

        if (hitBox == null)
        {
            Debug.LogError("BoxCollider hitBox no est� asignado. Por favor, asignalo desde el editor.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!ATK)
        {
            if (regenTimer > 0f)
            {
                regenTimer -= Time.deltaTime;

            }
            else
            {
                Stamina = Mathf.Clamp(Stamina + 0.05f, 0, 100);
                //Debug.Log(Stamina + "valor actual");
            }
        }
        else
        {
            regenTimer = 2f;
        }


        if (Input.GetKeyDown(KeyCode.Q)) 
        {
            if (_lock) 
            {
                _lock = false;
                camera_Controller.Play("FreeLook");
            }
            else 
            {
                _lock = true;
                camera_Controller.Play("Lock");
            }

        }

        if (staminaSlider != null) 
        {
            staminaSlider.value = Stamina / 100f; 
        }


    }

    public void GetInpur(InputAction.CallbackContext context) 
    {
        if (ATK) return;

        Vector2 Input = context.ReadValue<Vector2>();

        PlayerRotation();

        character_Controller.SetFloat("X", Input.x);
        character_Controller.SetFloat("Y", Input.y);
    }

    private void PlayerRotation() 
    {
        float angle = Mathf.Atan2(cam.forward.x,cam.forward.z) * Mathf.Rad2Deg;

        //Debug.Log(angle);
        transform.rotation = Quaternion.Euler(0,angle,0);
    }

    public void WeakATK(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started) return;

        if (!ATK)
        {
            if (Stamina >= 10f)
            {
                ATK = true;
                character_Controller.Play(ATK_Sequence_Weak[weak_ATK_Index]);
                weak_ATK_Index++;
                Stamina -= 10;
                Debug.Log(Stamina + "ataque realizado valor actual");
            }
                
        }
        else
        {
            if (Stamina >= 10f)
            {
                weakSequence = true;
                Stamina -= 10;
                Debug.Log(Stamina + "ataque realizado valor actual");
            }
               
        }   
    }

    public void Check_For_Weak_Sequence()
    {
        if (weakSequence)
        {
            weakSequence = false;
            character_Controller.Play(ATK_Sequence_Weak[weak_ATK_Index]);
            weak_ATK_Index++;
        }
    }

    public void End_Weak_Sequence()
    {
        weak_ATK_Index = 0;
        weakSequence = false;
        ATK = false;
    }

    public void StrongATK(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started) return;

        if (!ATK)
        {
            if (Stamina >= 15f) 
            {
                ATK = true;
                character_Controller.Play(ATK_Sequence_Strong[strong_ATK_Index]);
                strong_ATK_Index++;
                Stamina -= 15;
                Debug.Log(Stamina + " ataque fuerte realizado ");
            }
        }
        else
        {
            if (Stamina >= 15f)
            {
                strongSequence = true;
                Stamina -= 15;
                Debug.Log(Stamina + "ataque fuerte realizado ");
            }
                
        }
    }

    public void Check_For_Strong_Sequence()
    {
        if (strongSequence)
        {
            strongSequence = false;
            character_Controller.Play(ATK_Sequence_Strong[strong_ATK_Index]);
            strong_ATK_Index++;
        }
    }

    public void End_Strong_Sequence()
    {
        strong_ATK_Index = 0;
        strongSequence = false;
        ATK = false;
    }

    public void Ulti(InputAction.CallbackContext context)
    {
        if (Stamina >= 90f)
        {
            character_Controller.Play("Ulti");
            Stamina -= 90;
            Debug.Log("ulti realizada");
        }
            
    }

    public void End_AllSequence() 
    {
        End_Strong_Sequence();
        End_Weak_Sequence();
    }
    public void RecibirDa�o(float damage, Vector3 hitPoint)
    {
        if (isInvulnerable) return;

        Hp -= damage;
        Hp = Mathf.Clamp(Hp, 0, 200); 
        Debug.Log("Da�o recibido: " + damage + " - HP actual: " + Hp);
        if (hitBox == null)
        {
            Debug.LogError("BoxCollider hitBox no est� asignado.");
            return;
        }

        Vector3 localHitPoint = transform.InverseTransformPoint(hitPoint);
        Debug.Log("Local Hit Point: " + localHitPoint);

        if (localHitPoint.z > 0.5f * hitBox.size.z)
        {
            character_Controller.Play("DamageFront");
            Debug.Log("front");
        }
        else if (localHitPoint.z < -0.5f * hitBox.size.z)
        {
            character_Controller.Play("DamageBack");
            Debug.Log("back");
        }
        else if (localHitPoint.x > 0.5f * hitBox.size.x)
        {
            character_Controller.Play("DamageRight");
            Debug.Log("right");
        }
        else if (localHitPoint.x < -0.5f * hitBox.size.x)
        {
            character_Controller.Play("DamageLeft");
            Debug.Log("left");
        }
        StartCoroutine(ActivateInvulnerability());
        /*
      
        Vector3 hitDirection = (hitPoint - transform.position).normalized;
        float angle = Mathf.Atan2(hitDirection.x, hitDirection.z) * Mathf.Rad2Deg;
        float relativeAngle = Mathf.DeltaAngle(transform.eulerAngles.y, angle);
        //character_Controller.SetFloat("HitAngle", angle);
        Debug.Log("HitAngle: " + relativeAngle);
        //character_Controller.Play("Damage");
        if (relativeAngle >= -200 && relativeAngle <= -1)
        {
            character_Controller.Play("Damage");
        }
        else if (relativeAngle > 110 && relativeAngle <= 300)
        {
            character_Controller.Play("Damage 0");
        }
        else if (relativeAngle >= 0 && relativeAngle <= 20)
        {
            character_Controller.Play("Damage 1");
        }
        else if (relativeAngle >= 21 && relativeAngle <= 109)
        {
            character_Controller.Play("Damage 2");
        }
        */


        if (Hp <= 0)
        {
            
            Debug.Log("El personaje ha muerto");
            ragdollController.SetRagdollState(true);

        }
    }
    private IEnumerator ActivateInvulnerability()
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(iFrames);
        isInvulnerable = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trap"))
        {
            Vector3 hitPoint = other.ClosestPoint(transform.position);
            RecibirDa�o(20f, hitPoint);
        }
    }

}
