using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Rigidbody[] rigidbodies;

    IEnumerator ActivateRigidBodies(bool activated)
    {
        yield return new WaitForEndOfFrame(); // Corrected: it should be WaitForEndOfFrame
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = !activated; // Corrected: it's isKinematic, not IsKinematic
        }
    }

    public void SetRagdollState(bool activated) // Corrected: bool should be lowercase
    {
        anim.enabled = !activated;
        StartCoroutine(ActivateRigidBodies(activated));
    }
}